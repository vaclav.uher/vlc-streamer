#!/bin/sh

echo "cvlc -I rc --rc-host ${VLC_RC_HOST_SERVER}:${VLC_RC_PORT} --loop --sout-keep --extraintf http --http-port ${VLC_HTTP_PORT} --http-password ${VLC_HTTP_PASSWORD} ${VLC_DEFAULT_VIDEO_PATH} --sout #rtp{dst=${STREAM_PUBLIC_IP},port=${STREAM_PUBLIC_PORT},mux=ts,sdp=${RTSP_URL}}"
cvlc -I rc --rc-host ${VLC_RC_HOST_SERVER}:${VLC_RC_PORT} --loop --sout-keep --extraintf http --http-port ${VLC_HTTP_PORT} --http-password ${VLC_HTTP_PASSWORD} ${VLC_DEFAULT_VIDEO_PATH} --sout \#rtp{dst=${STREAM_PUBLIC_IP},port=${STREAM_PUBLIC_PORT},mux=ts,sdp=${RTSP_URL}}