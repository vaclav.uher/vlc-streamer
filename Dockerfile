FROM alpine:3.10
RUN apk update
RUN apk add --no-cache vlc


RUN mkdir -p /srv/data/clips
RUN mkdir /srv/data/municipal_radio
RUN mkdir /srv/data/presentations

COPY run_vlc.sh /srv/
COPY test.mp4 /srv/data/test/

VOLUME /srv/data/clips /srv/data/municipal_radio /srv/data/presentations /srv/data/test/

WORKDIR /srv

EXPOSE 8080/tcp
EXPOSE 8081/tcp
EXPOSE 8081/udp
EXPOSE 8888/tcp
EXPOSE 1234/tcp
EXPOSE 1234/udp

CMD ["sh", "run_vlc.sh"]